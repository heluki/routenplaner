package util;

import java.util.Scanner;

//Class responsible for requesting user inputs
public class InputRequestHandler {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static String requestString(String input) {
        System.out.print(input);
        return SCANNER.nextLine();
    }
}
