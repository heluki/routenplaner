import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

//This class includes the Dijkstra Algorithm
public class DijkstraAlgorithm {

    public static void calculateShortestPathFromStart(City start) {
        //Distance to starting city is obviously 0 by default
        start.setDistance(0);

        //Cities where the Shortest path to from the start are known
        Set<City> settledNodes = new HashSet<>();
        //Cities where the Shortest path to from the start are unknown
        Set<City> unsettledNodes = new HashSet<>();

        unsettledNodes.add(start);

        //recursive until all cities are settled
        while (unsettledNodes.size() != 0) {
            //get lowest distance city from the unsettledNodes
            City currentCity = getLowestDistanceCity(unsettledNodes);
            unsettledNodes.remove(currentCity);
            //Map.Entry is a map whose contents are only obtainable through an iteration - like here
            for (Map.Entry<City, Integer> adjacencyPair :
                    currentCity.getAdjacentCities().entrySet()) {
                City adjacentCity = adjacencyPair.getKey();
                Integer value = adjacencyPair.getValue();
                //if adjacent cities have not jet been settled, do that now
                if (!settledNodes.contains(adjacentCity)) {
                    calculateMinimumDistance(adjacentCity, value, currentCity);
                    unsettledNodes.add(adjacentCity);
                }
            }
            //cities that have their distance and shortest paths set in stone are settled
            settledNodes.add(currentCity);
        }
    }
//Calculates distance from start to adjacent city,
// sets it, and adds it to shortestDistance list (if it's a faster path)
    private static void calculateMinimumDistance(City adjacentCity,
                                                 Integer value, City currentCity) {
        Integer sourceDistance = currentCity.getDistance();
        if (sourceDistance + value < adjacentCity.getDistance()) {
            adjacentCity.setDistance(sourceDistance + value);
            LinkedList<City> shortestPath = new LinkedList<>(currentCity.getShortestPath());
            shortestPath.add(currentCity);
            adjacentCity.setShortestPath(shortestPath);
        }
    }

    //return lowest distance city from the unsettledNodes
    private static City getLowestDistanceCity(Set<City> unsettledNodes) {
        City lowestDistanceNode = null;
        //Integer.MAX_VALUE is used to simulate an infinite distance(used as default value).
        int lowestDistance = Integer.MAX_VALUE;
        //Iterate through unsettledNodes and return lowest distance city
        for (City city : unsettledNodes) {
            int nodeDistance = city.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = city;
            }
        }
        return lowestDistanceNode;
    }
}
