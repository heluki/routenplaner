import java.util.HashSet;
import java.util.Set;

//This Class represents the Graph from the problem
public class Graph {

    private Set<City> cities = new HashSet<>();

    public void addNode(City city) {
        cities.add(city);
    }
}
