
import java.util.*;

//This Class represents a City - or a node - from the Graph
public class City {

    private String name;

    //List describing shortest path from Start
    private List<City> shortestPath = new LinkedList<>();

    //Value for distance from starting point
    //Integer.MAX_VALUE is used to simulate an infinite distance(used as default value).
    private Integer distance = Integer.MAX_VALUE;

    Map<City, Integer> adjacentCities = new HashMap<>();

    public City(String name) {
        this.name = name;
    }

    public void addDestination(City destination, int distance) {
        adjacentCities.put(destination, distance);
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Map<City, Integer> getAdjacentCities() {
        return adjacentCities;
    }

    public List<City> getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(List<City> shortestPath) {
        this.shortestPath = shortestPath;
    }

    public String getName() {
        return name;
    }
}
