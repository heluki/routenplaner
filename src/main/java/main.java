import util.InputRequestHandler;

import java.util.*;

public class main {


    public static void main(String[] args) {

        //Adding dataset to Graph
        City SH = new City("SH");
        City KO = new City("KO");
        City BS = new City("BS");
        City WI = new City("WI");
        City SG = new City("SG");
        City ZH = new City("ZH");
        City VD = new City("VD");
        City BI = new City("BI");
        City LU = new City("LU");
        City CH = new City("CH");
        City CF = new City("CF");
        City NE = new City("NE");
        City BE = new City("BE");
        City TH = new City("TH");
        City GE = new City("GE");
        City LG = new City("LG");

        //adding adjacent cities
        GE.addDestination(NE, 2);
        GE.addDestination(TH, 2);
        GE.addDestination(LG, 5);
        NE.addDestination(CF, 1);
        NE.addDestination(BI, 1);
        NE.addDestination(BE, 1);
        NE.addDestination(GE, 2);
        CF.addDestination(NE, 1);
        CF.addDestination(BI, 1);
        BI.addDestination(CF, 1);
        BI.addDestination(BE, 1);
        BI.addDestination(BS, 2);
        BI.addDestination(ZH, 2);
        BE.addDestination(BI, 1);
        BE.addDestination(NE, 1);
        BE.addDestination(TH, 1);
        BE.addDestination(ZH, 2);
        LU.addDestination(ZH, 1);
        LU.addDestination(LG, 3);
        LU.addDestination(TH, 2);
        LG.addDestination(LU, 3);
        LG.addDestination(GE, 5);
        LG.addDestination(CH, 3);
        CH.addDestination(LG, 3);
        CH.addDestination(LU, 3);
        CH.addDestination(VD, 1);
        CH.addDestination(ZH, 2);
        VD.addDestination(CH, 1);
        VD.addDestination(SG, 1);
        SG.addDestination(VD, 1);
        SG.addDestination(KO, 1);
        SG.addDestination(WI, 1);
        KO.addDestination(SG, 1);
        KO.addDestination(SH, 1);
        KO.addDestination(WI, 1);
        SH.addDestination(KO, 1);
        SH.addDestination(WI, 1);
        SH.addDestination(BS, 2);
        BS.addDestination(SH, 2);
        BS.addDestination(ZH, 2);
        BS.addDestination(BI, 2);
        ZH.addDestination(BS, 2);
        ZH.addDestination(WI, 1);
        ZH.addDestination(CH, 2);
        ZH.addDestination(LU, 1);
        ZH.addDestination(BE, 2);
        ZH.addDestination(BI, 2);
        TH.addDestination(BE, 1);
        TH.addDestination(GE, 2);
        TH.addDestination(LU, 2);
        WI.addDestination(SH, 1);
        WI.addDestination(KO, 1);
        WI.addDestination(ZH, 1);

        Graph graph = new Graph();
        graph.addNode(SH);
        graph.addNode(KO);
        graph.addNode(BS);
        graph.addNode(WI);
        graph.addNode(SG);
        graph.addNode(ZH);
        graph.addNode(VD);
        graph.addNode(BI);
        graph.addNode(LU);
        graph.addNode(CH);
        graph.addNode(CF);
        graph.addNode(NE);
        graph.addNode(BE);
        graph.addNode(TH);
        graph.addNode(GE);
        graph.addNode(LG);

        //Asking for and handling user inputs
        String startInput = InputRequestHandler.requestString("Geben sie die Startstadt an (Als abkürzung zb: ZH) : ");
        switch (startInput) {
            case "SH":
                DijkstraAlgorithm.calculateShortestPathFromStart(SH);
                break;
            case "KO":
                DijkstraAlgorithm.calculateShortestPathFromStart(KO);
                break;
            case "BS":
                DijkstraAlgorithm.calculateShortestPathFromStart(BS);
                break;
            case "WI":
                DijkstraAlgorithm.calculateShortestPathFromStart(WI);
                break;
            case "SG":
                DijkstraAlgorithm.calculateShortestPathFromStart(SG);
                break;
            case "ZH":
                DijkstraAlgorithm.calculateShortestPathFromStart(ZH);
                break;
            case "VD":
                DijkstraAlgorithm.calculateShortestPathFromStart(VD);
                break;
            case "BI":
                DijkstraAlgorithm.calculateShortestPathFromStart(BI);
                break;
            case "LU":
                DijkstraAlgorithm.calculateShortestPathFromStart(LU);
                break;
            case "CH":
                DijkstraAlgorithm.calculateShortestPathFromStart(CH);
                break;
            case "CF":
                DijkstraAlgorithm.calculateShortestPathFromStart(CF);
                break;
            case "NE":
                DijkstraAlgorithm.calculateShortestPathFromStart(NE);
                break;
            case "BE":
                DijkstraAlgorithm.calculateShortestPathFromStart(BE);
                break;
            case "TH":
                DijkstraAlgorithm.calculateShortestPathFromStart(TH);
                break;
            case "GE":
                DijkstraAlgorithm.calculateShortestPathFromStart(GE);
                break;
            case "LG":
                DijkstraAlgorithm.calculateShortestPathFromStart(LG);
                break;
        }
        String goalInput = InputRequestHandler.requestString("Geben sie eine Zielstadt ein: ");
        //Displaying Results based on user input
        switch (goalInput) {
            case "SH":
                for (City city : SH.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "KO":
                for (City city : KO.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "BS":
                for (City city : BS.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "WI":
                for (City city : WI.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "SG":
                for (City city : SG.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "ZH":
                for (City city : ZH.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "VD":
                for (City city : VD.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "BI":
                for (City city : BI.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "LU":
                for (City city : LU.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "CH":
                for (City city : CH.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "CF":
                for (City city : CF.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "NE":
                for (City city : NE.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "BE":
                for (City city : BE.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "TH":
                for (City city : TH.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "GE":
                for (City city : GE.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
            case "LG":
                for (City city : LG.getShortestPath()) {
                    System.out.print(city.getName() + " ");
                }
                break;
        }
    }
}
